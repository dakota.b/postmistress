#ifndef ADDREMINDER_H
#define ADDREMINDER_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class AddReminder;
}

class AddReminder : public QDialog
{
    Q_OBJECT

public:
    explicit AddReminder(MainWindow *parent = nullptr);
    ~AddReminder();

private slots:
    void on_buttonBox_accepted();

    void on_AddReminder_destroyed(QObject *arg1);

private:
    Ui::AddReminder *ui;
    MainWindow *parent;

};

#endif // ADDREMINDER_H
