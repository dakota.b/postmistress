#ifndef NOTIFICATIONPASSABLE_H
#define NOTIFICATIONPASSABLE_H

#include <QDialog>
#include "notification.h"


namespace Ui {
class NotificationPassable;
}

class NotificationPassable : public QDialog
{
    Q_OBJECT

public:
    explicit NotificationPassable(QWidget *parent = nullptr, int notif = -1);
    ~NotificationPassable();

private slots:
    void on_buttonBox_accepted();

    void on_NotificationPassable_finished(int result);

private:
    Ui::NotificationPassable *ui;
    Notification *subject;
    bool didAccept = false;
    int subjectId = -1;
};

#endif // NOTIFICATIONPASSABLE_H
