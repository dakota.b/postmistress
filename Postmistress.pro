#-------------------------------------------------
#
# Project created by QtCreator 2019-06-21T08:05:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

TARGET = Postmistress
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

# Icon
RC_ICONS = Assets/img/icon.ico

SOURCES += \
        addreminder.cpp \
        alarmlisting.cpp \
        main.cpp \
        mainwindow.cpp \
        notification.cpp \
        notificationpassable.cpp \
        notificationprofile.cpp \
        notificationstyle.cpp

HEADERS += \
        addreminder.h \
        alarmlisting.h \
        declaration.h \
        mainwindow.h \
        notification.h \
        notificationpassable.h \
        notificationprofile.h \
        notificationstyle.h

FORMS += \
        addreminder.ui \
        alarmlisting.ui \
        mainwindow.ui \
        notificationpassable.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    Assets/img/Icon-128.png \
    Assets/img/Icon-16.png \
    Assets/img/Icon-24.png \
    Assets/img/Icon-256.png \
    Assets/img/Icon-32.png \
    Assets/img/Icon-64.png \
    Assets/img/Icon-Full.png \
    Assets/img/icon.ico \
    Assets/img/icon_w24.ico \
    config.xml \
    icon.ico

RESOURCES += \
    resources.qrc
