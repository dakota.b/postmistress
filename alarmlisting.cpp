#include "alarmlisting.h"
#include "ui_alarmlisting.h"

#include <QLabel>


AlarmListing::AlarmListing(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AlarmListing)
{
    ui->setupUi(this);
    // create a layout for each alarm

    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->addLayout(newLayout("Alarm Title", "June 30, 2019 21:57"));
    layout->addLayout(newLayout("Alorum", "July 1, 2019 21:57"));
    layout->addLayout(newLayout("Plane", "July 2, 2019 21:57"));
    layout->addLayout(newLayout("Alarm Title", "June 30, 2019 21:57"));
    layout->addLayout(newLayout("Alorum", "July 1, 2019 21:57"));
    layout->addLayout(newLayout("Plane", "July 2, 2019 21:57"));
    layout->addLayout(newLayout("Alarm Title", "June 30, 2019 21:57"));
    layout->addLayout(newLayout("Alorum", "July 1, 2019 21:57"));
    layout->addLayout(newLayout("Plane", "July 2, 2019 21:57"));
    layout->addLayout(newLayout("Alarm Title", "June 30, 2019 21:57"));
    layout->addLayout(newLayout("Alorum", "July 1, 2019 21:57"));
    layout->addLayout(newLayout("Plane", "July 2, 2019 21:57"));
    layout->addLayout(newLayout("Alarm Title", "June 30, 2019 21:57"));
    layout->addLayout(newLayout("Alorum", "July 1, 2019 21:57"));
    layout->addLayout(newLayout("Plane", "July 2, 2019 21:57"));
    QRect geoSize(0, 0, 1920, 5000);
    layout->setGeometry(geoSize);

    // TODO: figure this shit out

    this->ui->scrollArea->setLayout(layout);

}

QBoxLayout *AlarmListing::newLayout(QString description, QString date) {
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight, this);
    // create a new item for the title
    QLabel *title = new QLabel();
    // set the title
    title->setText(description);
    layout->addWidget(title); // add the title to the layout
    QSpacerItem *spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding);
    layout->addSpacerItem(spacer);
    QLabel *time = new QLabel();
    time->setText(date);
    layout->addWidget(time);
    QRect geoSize(0, 0, 1920, title->geometry().height()+12);
    layout->setGeometry(geoSize);
    return layout;
}

AlarmListing::~AlarmListing()
{
    delete ui;
}
