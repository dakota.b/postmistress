#include "notificationpassable.h"
#include "ui_notificationpassable.h"
#include "notificationprofile.h"

#include <iostream>
#include <QSound>
#include <QMessageBox>
#include "mainwindow.h"

NotificationPassable::NotificationPassable(QWidget *parent, int notif) :
    QDialog(parent),
    ui(new Ui::NotificationPassable)
{
    QSound::play("C:/Users/Dakota/Documents/dev/qt/Examples/Qt-5.13.0/demos/maroon/content/audio/catch.wav");
    this->subject = NotificationProfile::getNotification(notif);
    this->subjectId = notif;
    ui->setupUi(this);
    ui->notificationText->setText(this->subject->getDescription());
    // TODO: set up stylesheets class
    this->setStyleSheet(QString("background-color: %0;").arg(this->subject->getColor()));
}

NotificationPassable::~NotificationPassable()
{
    delete ui;
}

void NotificationPassable::on_buttonBox_accepted()
{
    this->didAccept = true;
    switch (this->ui->postponeSelect->currentIndex()) {
    case 0:
        // remove the option from the list and exit
        NotificationProfile::removeNotificationAndSave(this->subjectId);
        break;
    case 1:
        // postpone the option 10 minutes and exit
        //Notification n = *this->subject;
        this->subject->addSeconds(600);
        NotificationProfile::setNotificationAt(this->subjectId, this->subject);
        NotificationProfile::save();
        break;
    case 2:
        // postpone the option 20 minutes and exit
        this->subject->addSeconds(1200);
        NotificationProfile::setNotificationAt(this->subjectId, this->subject);
        NotificationProfile::save();
        break;
    case 3:
        // postpone the option 1 hour and exit
        this->subject->addSeconds(3600);
        NotificationProfile::setNotificationAt(this->subjectId, this->subject);
        NotificationProfile::save();
        break;
    case 4:
        // postpone the option 3 hours and exit
        this->subject->addSeconds(10800);
        NotificationProfile::setNotificationAt(this->subjectId, this->subject);
        NotificationProfile::save();
        break;
    case 5:
        // postpone the option til tomorrow and exit
        break;
    }
    dynamic_cast<MainWindow*>(parent())->updateTable();

}

void NotificationPassable::on_NotificationPassable_finished(int result)
{
    if (!this->didAccept)
    {
        // if the user cancels or clicks "x", add an hour
        this->subject->addSeconds(3600);
        NotificationProfile::setNotificationAt(this->subjectId, this->subject);
        NotificationProfile::save();
    }
    result = 2;
    NotificationProfile::windowActive = -1;
}
