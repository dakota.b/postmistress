#include "notificationstyle.h"

NotificationStyle::NotificationStyle()
{

}

QString NotificationStyle::toQString(int r, int g, int b) {
    return (new QString())->sprintf("#%02x%02x%02x", r, g, b);
}

void NotificationStyle::setBGColor(QString bgColor) {
    this->bgColor = (new QString(NSTYLE_BG_COLORSTR))->arg(bgColor);
}

void NotificationStyle::setButtonColor(QString buttonColor) {
    //this->bgColor = (new QString(NSTYLE_BG_COLORSTR))->arg(buttonColor);
    // TODO: fill in button color pointing attriute
}

void NotificationStyle::setTextColor(QString textColor) {
    this->textColor = (new QString(NSTYLE_TEXT_COLORSTR))->arg(textColor);
}

void NotificationStyle::setBGColor(int r, int g, int b) {
    this->setBGColor(this->toQString(r, g, b));
}

void NotificationStyle::setButtonColor(int r, int g, int b) {
    this->setButtonColor(this->toQString(r, g, b));
}

void NotificationStyle::setTextColor(int r, int g, int b) {
    this->setTextColor(this->toQString(r, g, b));
}

QString NotificationStyle::toString() {
    if (this->bgColor == nullptr) {
        this->bgColor = "";
    }

    if (this->textColor == nullptr) {
        this->textColor = "";
    }

    if (this->buttonColor == nullptr) {
        this->buttonColor = "";
    }

    return (new QString("%0 %1 %2"))->arg(this->bgColor, this->textColor, this->buttonColor);
}
