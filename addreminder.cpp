#include "addreminder.h"
#include "ui_addreminder.h"
#include "notificationprofile.h"
#include "declaration.h"
#include "mainwindow.h"

#include <iostream>
#include <QString>

AddReminder::AddReminder(MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::AddReminder)
{
    ui->setupUi(this);
    this->setModal(true);
    this->parent = parent;
    // can't put in a time that is less than now!
    ui->dueTimeInput->setMinimumDateTime(QDateTime::currentDateTime());
    ui->dueTimeInput->setDateTime(QDateTime::currentDateTime());
    ui->dueTimeInput->setCalendarPopup(true);
}

AddReminder::~AddReminder()
{
    delete ui;
}

void AddReminder::on_buttonBox_accepted()
{
    // TODO: figure out why the program exits when this is accepted...
    QString description = this->ui->descriptionInput->text();
    QDateTime dateTime = this->ui->dueTimeInput->dateTime();
    int color = this->ui->colorInput->currentIndex();
    bool isImportant = !this->ui->passableInput->isChecked();

    Notification* note = new Notification(description, dateTime, color, isImportant);
    NotificationProfile::addNotificationAndSave(*note);
    this->parent->updateTable();
    // write the data to the database. consider an API that you can send a JSON obj to?
}

/*
 * // WIP
void AddReminder::closeEvent(QCloseEvent *event)
{
    if(dynamic_cast<MainWindow*>(this->parent())->closing)
    {
        event->accept();
    }
    else
    {
        this->hide();
        event->ignore();
    }
}
*/
void AddReminder::on_AddReminder_destroyed(QObject *arg1)
{

}
