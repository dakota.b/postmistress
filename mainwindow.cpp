#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "addreminder.h"
#include "alarmlisting.h"
#include <QCloseEvent>
#include <QAction>
#include <QMenu>

#define TBL_COLS 3

QDateTime MainWindow::startTime = (new QDateTime())->currentDateTime();

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qApp->setQuitOnLastWindowClosed(false);

    closing = false;

    // right click tray icon -> exit
    QAction *exitAction = new QAction(tr("&Exit"), this);
    connect(exitAction, &QAction::triggered, [this]() {
        closing = true;
        close();
    });

    QMenu *trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(exitAction);

    // right click tray icon -> new alarm
    QAction *newAlarmAction = new QAction(tr("&New Alarm"), this);
    connect(newAlarmAction, &QAction::triggered, [this]() {
        AddReminder *ar = new AddReminder(this);
        ar->show();
    });
    trayIconMenu->addAction(newAlarmAction);

    QAction *showMainAction = new QAction(tr("&Open"), this);
    connect(showMainAction, &QAction::triggered, [this]() {
        this->show();
    });
    trayIconMenu->addAction(showMainAction);

    QSystemTrayIcon *sysTrayIcon = new QSystemTrayIcon(this);
    sysTrayIcon->setContextMenu(trayIconMenu);
    sysTrayIcon->setIcon(QIcon(":/icons/Assets/img/icon_w24.ico"));
    sysTrayIcon->connect(sysTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this,
                        SLOT(restoreWindowTrigger(QSystemTrayIcon::ActivationReason)));
    sysTrayIcon->show();


    //this->button = new QWinTaskbarButton(this);
    //button->setWindow(this->windowHandle());

    // loads the notifications from the database
    NotificationProfile::loadJson(DB_FILE);

    // setup of table parts that won't change
    this->ui->alarmsTable->setColumnCount(TBL_COLS);
    this->ui->alarmsTable->setHorizontalHeaderLabels(QStringList() << "Alarm Title" << "Timer" << "Important");
    ui->alarmsTable->setSelectionBehavior(QTableWidget::SelectRows);

    this->updateTable();

    // begins the loop to check notifications every second
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, QOverload<>::of(&MainWindow::updateTable));
    timer->start(60000);
}

// this method is called when the user double-clicks the tray icon
void MainWindow::restoreWindowTrigger(QSystemTrayIcon::ActivationReason reason)
{
    if(reason == QSystemTrayIcon::DoubleClick)
    {
        show();
        activateWindow();
    }
}

MainWindow::~MainWindow()
{
    std::cout << "Mainwindow Destructed" << std::endl;
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(closing)
    {
        qApp->setQuitOnLastWindowClosed(true);

        event->accept();
    }
    else
    {
        this->hide();
        event->ignore();
    }
}

void MainWindow::updateTable()
{
    this->ui->alarmsTable->setRowCount(NotificationProfile::count());

    QStandardItemModel model(NotificationProfile::count(), TBL_COLS);

    // generate a row in the table for each notification
    // also, check if that notification is due and launch it if so
    for (int row = 0; row < NotificationProfile::count(); row++) {
        generateRow(row);
        launchNotificationIfDue(row);
    }
    this->ui->statusLabel->setText(QString("Total notifications set: %0").arg(NotificationProfile::count()));
}

void MainWindow::generateRow(int rowNum) {
    Notification *notif = NotificationProfile::getNotification(rowNum);
    QTableWidgetItem *desc = new QTableWidgetItem(notif->getDescription());
    this->ui->alarmsTable->setItem(rowNum, 0, desc);

    // calculate seconds until event
    qint64 secs = QDateTime::currentDateTime().secsTo(notif->getAlertTime());

    // format the string as hh:mm:ss
    QString timeStr = "DUE";
    if (secs > 0) {
        char d[20];
        sprintf(d, "%02d:%02d", static_cast<int>(secs/3600), static_cast<int>((secs/60)%60));
        timeStr = *new QString(d);
    }

    // add it to the table
    QTableWidgetItem *date = new QTableWidgetItem(timeStr);
    this->ui->alarmsTable->setItem(rowNum, 1, date);

    // add whether the notification is important
    QTableWidgetItem *importance = new QTableWidgetItem(notif->isImportant() ? "X" : "");
    this->ui->alarmsTable->setItem(rowNum, 2, importance);
}

QDateTime MainWindow::tomorrow(int h, int m) {
    return QDateTime::fromSecsSinceEpoch((new QDateTime(QDate::currentDate().addDays(1)))->toSecsSinceEpoch() + h*3600 + m*60);
}

void MainWindow::launchNotificationIfDue(int notifNumber) {
    Notification *currentNotif = NotificationProfile::getNotification(notifNumber);

    QDateTime cdt = QDateTime::currentDateTime();

    if ( ( currentNotif->getAlertTime() == cdt || (currentNotif->getAlertTime() < cdt && MainWindow::startTime.secsTo(cdt) > WAKEUP_MINS*60))
            && NotificationProfile::windowActive == -1
       ) {
        // TODO: focus notification windows automatically

        qApp->alert(this, 0);

        if (currentNotif->isImportant()) {
            // launch important notification window
            // TODO: remove temporary code
            // **** temporary code ****
            NotificationProfile::windowActive = notifNumber;
            NotificationPassable *np = new NotificationPassable(this, notifNumber);
            np->show();
            // **** end temporary code ****
        } else {
            NotificationProfile::windowActive = notifNumber;
            NotificationPassable *np = new NotificationPassable(this, notifNumber);
            // TODO: figure out a way to do this without availableGeometry
            QRect rec = QDesktopWidget().availableGeometry();
            // DEBUG: std::cout << rec.width() << "," << rec.height() << std::endl;
            np->show();
            np->move(rec.width() - np->frameGeometry().width(), rec.height() - np->frameGeometry().height());
            // DEBUG
            //AlarmListing *al = new AlarmListing(this);
            //al->show();
            // END DEBUG
        }
        // TODO: control which stylesheet is loaded based on the color set for the notification
    }
}

void MainWindow::on_addNotification_clicked()
{
    AddReminder *ar = new AddReminder(this);
    ar->show();
}

void MainWindow::on_removeButton_released()
{
       QModelIndexList li = ui->alarmsTable->selectionModel()->selectedRows();
       for (int i = 0; i < li.count(); i++) {
           QModelIndex index = li.at(i);
           NotificationProfile::removeNotificationAndSave(index.row());
       }
       updateTable();
}
