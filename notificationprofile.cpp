#include "notificationprofile.h"
#include "declaration.h"

NotificationProfile::NotificationProfile()
{

}

QJsonArray NotificationProfile::notifications = *(new QJsonArray());
int NotificationProfile::windowActive = -1;
QList<QString> *NotificationProfile::colors = new QList<QString> {
        COLORS_RED,
        COLORS_GREEN,
        COLORS_BLUE,
        COLORS_YELLOW,
        COLORS_PINK,
        COLORS_PURPLE,
        COLORS_ORANGE };

void NotificationProfile::loadJson(QString fileName) {
    QFile jsonFile(fileName);
    jsonFile.open(QFile::ReadOnly);
    QJsonDocument d = QJsonDocument().fromJson(jsonFile.readAll());
    NotificationProfile::notifications = d.array();
}

void NotificationProfile::saveJson(QJsonDocument document, QString fileName) {
    QFile jsonFile(fileName);
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write(document.toJson());
}

void NotificationProfile::save()
{
    QJsonDocument *d = new QJsonDocument();
    d->setArray(NotificationProfile::notifications);
    NotificationProfile::saveJson(*d, DB_FILE);
}

void NotificationProfile::addNotificationAndSave(Notification n)
{
    NotificationProfile::notifications.push_back(n.toJson());
    NotificationProfile::save();
}

void NotificationProfile::removeNotificationAndSave(int n)
{
    NotificationProfile::notifications.removeAt(n);
    NotificationProfile::save();
}

QString NotificationProfile::printNotifications()
{
    QJsonDocument doc;
    doc.setArray(NotificationProfile::notifications);

    QString dataToString(doc.toJson());
    return dataToString;
}

int NotificationProfile::count()
{
    return NotificationProfile::notifications.size();
}

Notification *NotificationProfile::getNotification(int index)
{
    QJsonObject notif = NotificationProfile::notifications.at(index).toObject();
    return new Notification(notif);
}

void NotificationProfile::setNotificationAt(int index, Notification *notif)
{
    NotificationProfile::notifications.replace(index, notif->toJson());
}

