#ifndef ALARMLISTING_H
#define ALARMLISTING_H

#include <QWidget>
#include <QBoxLayout>


namespace Ui {
class AlarmListing;
}

class AlarmListing : public QWidget
{
    Q_OBJECT

public:
    explicit AlarmListing(QWidget *parent = nullptr);
    QBoxLayout *newLayout(QString desc, QString date);
    ~AlarmListing();

private:
    Ui::AlarmListing *ui;
};

#endif // ALARMLISTING_H
