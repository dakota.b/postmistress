#ifndef DECLARATION_H
#define DECLARATION_H

#define DB_FILE ".postmistress.db"
#define COL_COUNT 3
#define ROW_COUNT 10

// TODO: build all this together in a JSON file and add it to .postmistress.cfg
// TODO: and add a panel for it

// colors
#define COLORS_RED "#ffbbbb"
#define COLORS_GREEN "#98fb98"
#define COLORS_BLUE "#f0f8ff"
#define COLORS_YELLOW "#ffffe0"
#define COLORS_PINK "#ffc0cb"
#define COLORS_PURPLE "#e6e6fa"
#define COLORS_ORANGE "#ffdab9"

// time
#define DAY_H 8
#define DAY_M 0
#define WAKEUP_MINS 0

#endif // DECLARATION_H
