#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <QString>
#include <QDateTime>
#include <QJsonObject>

class Notification
{
public:
    Notification(QString description, QDateTime alertTime, int colorId, bool important);
    Notification(QJsonObject o);
    QString getDescription();
    QDateTime getAlertTime();
    QString getColor();
    void addSeconds(int secs);
    int getColorId();
    bool isImportant();
    QJsonObject toJson();
private:
    QString description;
    QDateTime alertTime;
    int colorId;
    bool important;
};

#endif // NOTIFICATION_H
