#ifndef NOTIFICATIONPROFILE_H
#define NOTIFICATIONPROFILE_H

#include <QJsonDocument>
#include <QFile>
#include <QJsonArray>
#include <notification.h>

class NotificationProfile
{
public:
    static void loadJson(QString fileName);
    static void saveJson(QJsonDocument document, QString fileName);
    static void addNotificationAndSave(Notification n);
    static void removeNotificationAndSave(int notif_id);
    static QString printNotifications();
    static int count();
    static void save();
    static int windowActive;
    static void setNotificationAt(int index, Notification *notif);
    static Notification *getNotification(int index);
    NotificationProfile();
    static QList<QString> *colors;
private:
    static QJsonArray notifications;
};

#endif // NOTIFICATIONPROFILE_H
