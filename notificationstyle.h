#ifndef NOTIFICATIONSTYLE_H
#define NOTIFICATIONSTYLE_H

#include <QString>

#define NSTYLE_BG_COLORSTR "background-color: %0;"
// #define NSTYLE_BUTTON_COLORSTR
 #define NSTYLE_TEXT_COLORSTR "color: %0;"

class NotificationStyle
{
public:
    NotificationStyle();
    void setBGColor(QString rgb);
    void setBGColor(int r, int g, int b);
    void setButtonColor(QString rgb);
    void setButtonColor(int r, int g, int b);
    void setTextColor(QString rgb);
    void setTextColor(int r, int g, int b);
    QString name;
    QString toString();
private:
    QString bgColor = nullptr;
    QString buttonColor = nullptr;
    QString textColor = nullptr;
    QString toQString(int r, int g, int b);
};

#endif // NOTIFICATIONSTYLE_H
