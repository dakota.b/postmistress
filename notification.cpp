#include "notification.h"

#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <iostream>
#include <notificationprofile.h>

Notification::Notification(QString description, QDateTime alertTime, int colorId, bool important)
{
    this->description = description;
    this->alertTime = alertTime;
    this->colorId = colorId;
    this->important = important;
}

Notification::Notification(QJsonObject o)
{
    this->description = o.take("description").toString();
    this->alertTime = QDateTime::fromSecsSinceEpoch(o.take("alertTime").toInt());
    this->colorId = o.take("colorId").toInt();
    this->important = o.take("important").toBool();
}

QJsonObject Notification::toJson()
{
    QJsonObject o {
        {"description", this->description},
        {"alertTime", this->alertTime.toSecsSinceEpoch()},
        {"colorId", this->colorId},
        {"important", this->important}
    };

    return o;
}

QString Notification::getDescription()
{
    return this->description;
}

void Notification::addSeconds(int seconds) {
    std::cout << this->alertTime.toString().toStdString() << std::endl;
    this->alertTime = this->alertTime.fromSecsSinceEpoch(this->alertTime.currentDateTime().toSecsSinceEpoch() + seconds);
    std::cout << this->alertTime.toString().toStdString() << std::endl;
}

QString Notification::getColor() {
    return NotificationProfile::colors->at(this->colorId);
}

QDateTime Notification::getAlertTime()
{
    return this->alertTime;
}

bool Notification::isImportant()
{
    return this->important;
}
