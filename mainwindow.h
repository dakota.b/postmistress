#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "notification.h"
#include <QtWinExtras/QWinTaskbarButton>
#include "notificationprofile.h"
#include "notificationpassable.h"
#include "declaration.h"

#include <QStandardItemModel>
#include <QTableWidgetItem>
#include <QString>
#include <QDebug>
#include <QTimer>
#include <iostream>
#include <QDesktopWidget>
#include <QSystemTrayIcon>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    void updateTable();
    static QDateTime startTime;
    QDateTime tomorrow(int h, int m);
    ~MainWindow();

private slots:
    void on_addNotification_clicked();
    void on_removeButton_released();
    void restoreWindowTrigger(QSystemTrayIcon::ActivationReason reason);

private:
    bool closing;
    Ui::MainWindow *ui;
    QWinTaskbarButton* button = nullptr;
    void generateRow(int rowNum);
    void launchNotificationIfDue(int notifNumber);

protected:
    void closeEvent(QCloseEvent *event) override;
};

#endif // MAINWINDOW_H
